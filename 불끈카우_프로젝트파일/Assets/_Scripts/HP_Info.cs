﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HP_Info : MonoBehaviour
{
    private bool isPause = true; // 게임 종료시 Dragon 정지 

    public Animator _animator;
    bool DragonDie;

    private float HP = 100;
    private float damage = 0.005f;

    public Image HPBar;
    public Image Mask;

    public GameObject panel;
    public Image GameOver;
    public Button StartBtn;
    public Button EndBtn;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;

        Mask.gameObject.SetActive(false);
        panel.gameObject.SetActive(false);
        GameOver.gameObject.SetActive(false);
        StartBtn.gameObject.SetActive(false);
        EndBtn.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        HP -= damage; //시간 흐름에 따라 HP 감소 
        HPBar.fillAmount = HP * 0.01f;

        if (HP < 0) // HP 0되면 죽음 
        {
            _animator.SetBool("DragonDie", true);

            StartCoroutine(TimeStop());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Fire") // 불에 닿았을 때 
        {
            damage = 0.007f;
        }

        if (other.tag == "Mask") // 방독면 먹었을 때 
        {
            damage = 0.001f;

            Mask.gameObject.SetActive(true);
            other.gameObject.SetActive(false);
        }

    }

    IEnumerator TimeStop()
    {
        yield return new WaitForSeconds(2.5f);

        panel.gameObject.SetActive(true);
        GameOver.gameObject.SetActive(true);
        StartBtn.gameObject.SetActive(true);
        EndBtn.gameObject.SetActive(true);

        Time.timeScale = 0;
        isPause = false; //정지
    }
}
