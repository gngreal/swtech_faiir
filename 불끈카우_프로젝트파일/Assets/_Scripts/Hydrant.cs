﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hydrant : MonoBehaviour
{
    public Image UseHD;
    public Button UseHDBtn;
    bool HDOK = true;

    // Start is called before the first frame update
    void Start()
    {
        UseHD.gameObject.SetActive(false);
        UseHDBtn.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "HD") // 소화기 먹었을 때 
        {
            Debug.Log("HDOK");

            if (HDOK == true)
            {
                UseHD.gameObject.SetActive(true);
                UseHDBtn.gameObject.SetActive(true);
                HDOK = false;
                other.gameObject.SetActive(false);
            }
        }
    }

    public void UseHDOK()
    {
        UseHD.gameObject.SetActive(false);
        UseHDBtn.gameObject.SetActive(false);
        HDOK = false;


    }

}
