﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public Button Next;
    public Button StartBtn;
    public Button RealStart;

    public Sprite[] _slide;
    public Image _image;
    int pageCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartBtn.gameObject.SetActive(true);
        Next.gameObject.SetActive(false);
        RealStart.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void imageSlide(int page)
    {
        _image.sprite = _slide[page];

    }

    public void Nextpage()
    {
        pageCount++;
        if(pageCount == 1)
        {
            StartBtn.gameObject.SetActive(false);
            Next.gameObject.SetActive(true);
        }

        if (pageCount >= 3)
        {
            Next.gameObject.SetActive(false);
            RealStart.gameObject.SetActive(true);
        }

        imageSlide(pageCount);
    }
}
